PawnGame.com Website
====================

Written in HTML, JS, CSS, and PHP.

### Licensing
The source code with a license header is licensed under MPL 2.0. 
See the file [LICENSE](./LICENSE) for full details.

There is no license provided for all other assets such as but not including to images.
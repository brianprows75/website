<?php

/*
 * Copyright © 2022  Alex
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

global $page_name;
global $page_location;
$page_name = "";
$page_location = "";
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <?php require ('src/elements/head.php') ?>
</head>

<body>
<?php require('src/elements/header.php') ?>

<?php require('src/elements/navigation.php') ?>


<table width="1035" cellspacing="0" cellpadding="0" border="0" align="center">
    <tbody>
    <tr valign="top">
        <td id="maincontent" width="850">
            <h2>Welcome back to PawnGame.com!</h2>
            <p>
                PawnGame.com is back! <br>
                Get started by <a href="https://pawngame.com/forum/register/">creating an account</a> on the forums. You can use your new forum username and password to play the <a href="https://www.pawngame.com/games.php">games</a>. <br>
            </p>
            <h2>Game Schedule</h2>
            <p>
                We currently play Pawn and Pawn Tactics weekly.<br>
                The next date we're playing on is: <br>
                <br>
                <b>Pawn</b>: <span class="main-feature" id="pawn-date"></span> <br>
                <span id="pawn-time"></span> <br>
                <b>Pawn Tactics</b>: <span class="main-feature" id="pawnt-date"></span> <br>
                <span id="pawnt-time"></span> <br>
                <br>
                See you there!
            </p>
            <h2>Discord</h2>
            <p>Connect with other players instantly by joining our <a href="https://discord.gg/XRyut9Z">Discord</a>.</p>
            <center>
                <iframe src="https://discord.com/widget?id=193377420643008513&theme=dark" width="350" height="500" allowtransparency="true" frameborder="0" sandbox="allow-popups allow-popups-to-escape-sandbox allow-same-origin allow-scripts"></iframe>
            </center>
        </td>
        <td width="185" valign="top" align="center">
        </td>
    </tr>
    </tbody>
</table>

<?php require('src/elements/footer.php') ?>
<script>
    {
        function getUpcomingUTCWeekDay(weekday) {
            let date = new Date();
            let currentDay = date.getUTCDay();
            if (currentDay > weekday) {
                let daysTilNextWeek = 7 - currentDay;
                date.setUTCDate(date.getUTCDate() + daysTilNextWeek);
                currentDay = date.getUTCDay();
            }
            if (currentDay === weekday) {
                return date;
            }
            if (currentDay < weekday) {
                date.setUTCDate(date.getUTCDate() + (weekday - currentDay));
                return date;
            }
            return date;
        }

        let nextPawnStartDate = new Date(getUpcomingUTCWeekDay(0));
        nextPawnStartDate.setUTCHours(22, 30, 0, 0);
        let nextPawnEndDate = new Date(nextPawnStartDate);
        nextPawnEndDate.setUTCHours(nextPawnEndDate.getUTCHours() + 2);

        let nextPawntStartDate = new Date(nextPawnEndDate);
        let nextPawntEndDate = new Date(nextPawntStartDate)
        nextPawntEndDate.setUTCHours(nextPawntEndDate.getUTCHours() + 2);

        document.getElementById("pawn-date").textContent = nextPawnStartDate.toLocaleDateString("en", {dateStyle: "full"});
        document.getElementById("pawn-time").textContent = nextPawnStartDate.toLocaleTimeString() + " - " + nextPawnEndDate.toLocaleTimeString();

        document.getElementById("pawnt-date").textContent = nextPawntStartDate.toLocaleDateString("en", {dateStyle: "full"});
        document.getElementById("pawnt-time").textContent = nextPawntStartDate.toLocaleTimeString() + " - " + nextPawntEndDate.toLocaleTimeString();
    }
</script>
</body>
</html>
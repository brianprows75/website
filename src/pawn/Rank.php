<?php



/*
 * Copyright © 2022  Alex
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

namespace PawnStudios;


class Rank
{
    private static $rank_list = [0, 1000, 2000, 3000, 4000, 5000, 7000, 9000, 11000, 13000, 15000, 20000, 25000, 30000, 35000, 40000, 50000, 60000, 70000, 80000, 90000, 140000, 190000, 240000, 290000, 340000, 440000, 540000, 640000, 740000, 840000, 1340000, 1840000, 2340000, 2840000, 4000000];
    private static $rank_images = ['0n00b.png', '1bar1.png', '1bar2.png', '1bar3.png', '1bar4.png', '1bar5.png', '2dbar1.png', '2dbar2.png', '2dbar3.png', '2dbar4.png', '2dbar5.png', '3crest1.png', '3crest2.png', '3crest3.png', '3crest4.png', '3crest5.png', '4star1.png', '4star2.png', '4star3.png', '4star4.png', '4star5.png', '5bstar1.png', '5bstar2.png', '5bstar3.png', '5bstar4.png', '5bstar5.png', '6tstar1.png', '6tstar2.png', '6tstar3.png', '6tstar4.png', '6tstar5.png', '7tcrest1.png', '7tcrest2.png', '7tcrest3.png', '7tcrest4.png', '7tcrest5.png'];
    private static $rank_titles = ['n00b', 'Private', 'Private, 1st Class', 'Second Lieutenant', 'First Lieutenant', 'Lieutenant', 'Specialist, Fourth Class', 'Specialist, Fifth Class', 'Specialist, Sixth Class', 'Specialist, First Class', 'Specialist', 'Captain', 'Major', 'Major Captain', 'Lt. Colonel', 'Colonel', 'Staff Sergeant', 'Sergeant, First Class', 'First Sergeant', 'Master Sergeant', 'Sergeant Major', 'Lt. Commander', 'Commander', 'Officer, Third Class', 'Officer, Second Class', 'Officer, First Class', 'Commodore', 'Rear Admiral', 'Vice Admiral', 'Admiral', 'Fleet Admiral', 'Brigadier General', 'Major General', 'Lieutenant General', 'General', 'General of the Army'];

    public $score;
    public $rank_image;
    public $rank_title;

    /**
     * Rank constructor.
     */
    public function __construct($score)
    {
        $this->score = $score;
        $this->rank_image = self::$rank_images[0];
        $this->rank_title = self::$rank_titles[0];

        foreach (self::$rank_list as $index => $rank_score) {
            if ($rank_score > $score) {
                $this->rank_image = self::$rank_images[$index];
                $this->rank_title = self::$rank_titles[$index];
                break;
            }
        }
    }
}
<?php

/*
 * Copyright © 2022  Alex
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

global $page_name;
global $page_location;
$page_name = "Pawn Tactics - Flash Player Projector Login Link";
$page_location = "ptlogin.php";

require('forum/src/XF.php');

XF::start("/");
$app = XF::setupApp('XF\Pub\App');
$app->run();
$session = $app->session();
$sid = $session->getSessionId();
$login_link = 'https://d1h3njoxaty54c.cloudfront.net/pawn-tactics/experimental/ptmain.swf?sid=' . $sid;
$userId = $session->get('userId');
$finder = XF::finder('XF:User');
$user = $finder->where('user_id', $userId)->fetchOne();
$username = null;
if (isset($user)) {
    $username = $user['username'];
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <?php require ('src/elements/head.php') ?>
</head>

<body>
<?php require('src/elements/header.php') ?>

<?php require('src/elements/navigation.php') ?>


<table width="1035" cellspacing="0" cellpadding="0" border="0" align="center">
    <tbody>
    <tr valign="top">
        <td id="maincontent" width="650">
            <h2>Pawn Tactics - Flash Player Projector Login Link</h2>
            <br>
            <p>This advanced guide will help you run Pawn Tactics from your desktop. It's intended to help players who are not using Windows.</p>
            <p>Note: To use your account ensure you're logged into the <a href="https://pawngame.com/forum/login/">forum</a> before visiting this page.</p>
            <p>You may need to visit this page for a new link after a period of time.</p>
            <br>
            <ol>
                <li>
                    <p>Run the Adobe Flash Player Projecter file you previously downloaded. Then click file -> open.</p>
                </li>
                <br>
                <li>
                    <p>Copy and paste the link provided below into the 'Location' input, then click 'OK'.</p>
                    <center><img alt="Flash Player Projector Location Window Image" src="https://d1h3njoxaty54c.cloudfront.net/website/landing/image/projector_location_image.png"></center>
                    <p><h3>WARNING: DO NOT SHARE THIS LINK WITH ANYONE. THIS IS YOUR PERSONAL LOGIN LINK TO THE GAME.</h3></p>
                    <?php
                    if (!isset($username)) {
                        echo '<p>You are not logged in, the following link will log you into Pawn Tactics as a guest.</p>';
                        echo '<p><a href="https://pawngame.com/forum/login/">Login to your account</a> and refresh the page to get your personal link.</p>';
                        echo '<p>Guest Login Link:</p>';
                        echo '<p style="color: #EDA200"><b>'.  $login_link  . '</b></p>';
                    } else {
                        echo '<p>' . $username . ' Login Link:</p>';
                        echo '<p style="color: #EDA200"><b>'.  $login_link  . '</b></p>';
                    }
                    ?>
                </li>
            </ol>
        </td>
    </tr>
    </tbody>
</table>

<?php require('src/elements/footer.php') ?>
</body>
</html>
<?php

/*
 * Copyright © 2022  Alex
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

$location = "https://pawngame.com/";
$title = "Pawn Game - Multiplayer Gaming";
$site_name = "Pawn Game - Multiplayer Gaming";
$description = "PawnGame.com is the home of two multiplayer games: Pawn (PG), and Pawn Tactics (PT).";
if ($page_location) {
    $location = $location . $page_location;
}
if ($page_name) {
    $title = $title . " - " . $page_name;
}
?>

<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title> <?php echo $title ?> </title>

<meta name="description" content="<?php echo $description ?>">

<meta property="og:description" content="<?php echo $description ?>" />
<meta property="twitter:description" content="<?php echo $description ?>" />

<meta property="og:site_name" content="<?php echo $site_name ?>" />
<meta property="og:type" content="website" />
<meta property="og:title" content="<?php echo $title ?>" />
<meta property="twitter:title" content="<?php echo $title ?>" />
<meta property="og:url" content="<?php echo $location ?>" />
<meta property="og:image" content="https://d1h3njoxaty54c.cloudfront.net/website/pawn_icon.png" />
<meta property="twitter:image" content="https://d1h3njoxaty54c.cloudfront.net/website/pawn_icon.png" />
<meta property="twitter:card" content="summary" />

<meta name="apple-mobile-web-app-title" content="Pawn Game Website">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black">

<link rel="apple-touch-icon" href="https://d1h3njoxaty54c.cloudfront.net/website/pawn_icon_dark_red_2.png" />
<link rel="canonical" href="<?php echo $location ?>" />
<link rel="icon" type="image/png" href="https://d1h3njoxaty54c.cloudfront.net/website/pawn_favicon.png" sizes="32x32" />

<link rel="stylesheet" type="text/css" href="https://pawngame.com/static/website/landing/style/style.css">
<?php

/*
 * Copyright © 2022  Alex
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

global $page_name;
global $page_location;
$page_name = "Pawn Tactics";
$page_location = "pawntactics.php";

require('forum/src/XF.php');

XF::start("/");
$app = XF::setupApp('XF\Pub\App');
$app->run();
$session = $app->session();
$sid = $session->getSessionId();
$user_id = $session->get('userId');
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <?php require ('src/elements/head.php') ?>
</head>

<body>
    <?php require('src/elements/header.php') ?>

    <?php require('src/elements/navigation.php') ?>


    <table width="1035" cellspacing="0" cellpadding="0" border="0" align="center">
        <tbody>
            <tr valign="top">
                <td id="maincontent" width="650">
                    <h2>Pawn Tactics</h2>

                    <div align="center">
                        <a href="https://pawngame.com/forum/threads/download-pawn-tactics-on-windows.1471/"><img src="https://pawngame.com/static/website/landing/image/playnow.gif" border="0"></a><br>
                        <a href="https://pawngame.com/forum/pages/pawntactics/">Alternate Link (Web Flash Player)</a>
                    </div>

                    <h2>Controls</h2>
                    <p>Movement</p>
                    <ul>
                        <li>W/Up - Move up</li>
                        <li>S/Down - Move down</li>
                        <li>A/Left - Move left</li>
                        <li>D/Right - Move right</li>
                        <li>Space/? - Toggle run</li>
                    </ul>
                    <p>Weapons</p>
                    <ul>
                        <li>1/Numpad 1 - Select weapon 1</li>
                        <li>2/Numpad 2 - Select weapon 2</li>
                        <li>3/Numpad 3 - Select weapon 3</li>
                        <li>4/Numpad 4 - Select weapon 4</li>
                        <li>Left Click - Primary Fire</li>
                        <li>F/Numpad 0 - Secondary Fire</li>
                        <li>Scroll Wheel - Cycle weapons</li>
                    </ul>
                    <p>Chat</p>
                    <ul>
                        <li>Enter - Show/Hide chat dialogue</li>
                        <li>Left/Right - Switch between whisper name and message</li>
                        <li>Shift + 0-9 - Send a radio command</li>
                    </ul>
                    <p>Other</p>
                    <ul>
                        <li>Right click (Windows)/Command + Click (Mac) - Access game menu to leave match</li>
                        <li>Q/&gt; - Show minimap</li>
                        <li>Esc - Exit Full Screen</li>
                    </ul>

                    <h2>Links</h2>
                    <ul>
                        <li><a href="https://pawngame.com/forum/forums/pawn-tactics-updates.86/" target="_blank">Changelog</a></li>
                        <li><a href="javascript:openMapTester()">Map Tester</a></li>
                        <li><a href="https://discord.gg/XRyut9Z" target="_blank">Join us on Discord!</a></li>
                    </ul>
                </td>
            </tr>
        </tbody>
    </table>

    <?php require('src/elements/footer.php') ?>

    <script type="text/javascript">
        function openGame() {
            var popup = window.open("playpt.php", "pawntactics", "width=" + screen.width + ", height=" + screen.height + ", scrollbars=no, resizable=yes, toolbar=no, location=no, status=no");
            if (popup.outerWidth < screen.availWidth || popup.outerHeight < screen.availHeight)
            {
                popup.moveTo(0,0);
                popup.resizeTo(screen.availWidth, screen.availHeight);
            }
        }
        function openMapTester() {
            var popup = window.open("playptmaptest.php", "pawntacticsmaptest", "width=" + screen.width + ", height=" + screen.height + ", scrollbars=no, resizable=yes, toolbar=no, location=no, status=no");
            if (popup.outerWidth < screen.availWidth || popup.outerHeight < screen.availHeight)
            {
                popup.moveTo(0,0);
                popup.resizeTo(screen.availWidth, screen.availHeight);
            }
        }
    </script>
</body>
</html>
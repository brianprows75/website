<?php

/*
 * Copyright © 2022  Alex
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

global $page_name;
global $page_location;
$page_name = "Pawn Rank List";
$page_location = "ranklist.php";
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <?php require ('src/elements/head.php') ?>
</head>

<body>
<?php require('src/elements/header.php') ?>

<?php require('src/elements/navigation.php') ?>

<table width="1035" cellspacing="0" cellpadding="0" border="0" align="center">
    <tbody>
    <tr valign="top">
        <td id="maincontent" width="650">
            <h2>Pawn Rank List</h2>
            <p>
                <img src="https://pawngame.com/static/website/landing/image/ranks/0n00b.png" alt="n00b image">&ensp;n00b: 0<br>

                <img src="https://pawngame.com/static/website/landing/image/ranks/1bar1.png" alt="Private image">&ensp;Private: 1,000<br>
                <img src="https://pawngame.com/static/website/landing/image/ranks/1bar2.png" alt="Private 1st image">&ensp;Private, 1st Class: 2,000<br>
                <img src="https://pawngame.com/static/website/landing/image/ranks/1bar3.png" alt="2nd lieut image">&ensp;Second Lieutenant: 3,000<br>
                <img src="https://pawngame.com/static/website/landing/image/ranks/1bar4.png" alt="1st lieut image">&ensp;First Lieutenant: 4,000<br>
                <img src="https://pawngame.com/static/website/landing/image/ranks/1bar5.png" alt="Liuet image">&ensp;Lieutenant: 5,000<br>
                <img src="https://pawngame.com/static/website/landing/image/ranks/2dbar1.png" alt="Specialist image">&ensp;Specialist, Fourth Class: 7,000<br>

                <img src="https://pawngame.com/static/website/landing/image/ranks/2dbar2.png" alt="Specialist 5th image">&ensp;Specialist, Fifth Class: 9,000<br>
                <img src="https://pawngame.com/static/website/landing/image/ranks/2dbar3.png" alt="Specilist 6th image">&ensp;Specialist, Sixth Class: 11,000<br>
                <img src="https://pawngame.com/static/website/landing/image/ranks/2dbar4.png" alt="rank icon image">&ensp;Specialist, First Class: 13,000<br>
                <img src="https://pawngame.com/static/website/landing/image/ranks/2dbar5.png" alt="rank icon image">&ensp;Specialist: 15,000<br>
                <img src="https://pawngame.com/static/website/landing/image/ranks/3crest1.png" alt="rank icon image">&ensp;Captain: 20,000<br>
                <img src="https://pawngame.com/static/website/landing/image/ranks/3crest2.png" alt="rank icon image">&ensp;Major: 25,000<br>

                <img src="https://pawngame.com/static/website/landing/image/ranks/3crest3.png" alt="rank icon image">&ensp;Major Captain: 30,000<br>
                <img src="https://pawngame.com/static/website/landing/image/ranks/3crest4.png" alt="rank icon image">&ensp;Lt. Colonel: 35,000<br>
                <img src="https://pawngame.com/static/website/landing/image/ranks/3crest5.png" alt="rank icon image">&ensp;Colonel: 40,000<br>
                <img src="https://pawngame.com/static/website/landing/image/ranks/4star1.png" alt="rank icon image">&ensp;Staff Sergeant: 50,000<br>
                <img src="https://pawngame.com/static/website/landing/image/ranks/4star2.png" alt="rank icon image">&ensp;Sergeant, First Class: 60,000<br>
                <img src="https://pawngame.com/static/website/landing/image/ranks/4star3.png" alt="rank icon image">&ensp;First Sergeant: 70,000<br>

                <img src="https://pawngame.com/static/website/landing/image/ranks/4star4.png" alt="rank icon image">&ensp;Master Sergeant: 80,000<br>
                <img src="https://pawngame.com/static/website/landing/image/ranks/4star5.png" alt="rank icon image">&ensp;Sergeant Major: 90,000<br>
                <img src="https://pawngame.com/static/website/landing/image/ranks/5bstar1.png" alt="rank icon image">&ensp;Lt. Commander: 140,000<br>
                <img src="https://pawngame.com/static/website/landing/image/ranks/5bstar2.png" alt="rank icon image">&ensp;Commander: 190,000<br>
                <img src="https://pawngame.com/static/website/landing/image/ranks/5bstar3.png" alt="rank icon image">&ensp;Officer, Third Class: 240,000<br>
                <img src="https://pawngame.com/static/website/landing/image/ranks/5bstar4.png" alt="rank icon image">&ensp;Officer, Second Class: 290,000<br>

                <img src="https://pawngame.com/static/website/landing/image/ranks/5bstar5.png" alt="rank icon image">&ensp;Officer, First Class: 340,000<br>
                <img src="https://pawngame.com/static/website/landing/image/ranks/6tstar1.png" alt="rank icon image">&ensp;Commodore: 440,000<br>
                <img src="https://pawngame.com/static/website/landing/image/ranks/6tstar2.png" alt="rank icon image">&ensp;Rear Admiral: 540,000<br>
                <img src="https://pawngame.com/static/website/landing/image/ranks/6tstar3.png" alt="rank icon image">&ensp;Vice Admiral: 640,000<br>
                <img src="https://pawngame.com/static/website/landing/image/ranks/6tstar4.png" alt="rank icon image">&ensp;Admiral: 740,000<br>
                <img src="https://pawngame.com/static/website/landing/image/ranks/6tstar5.png" alt="rank icon image">&ensp;Fleet Admiral: 840,000<br>

                <img src="https://pawngame.com/static/website/landing/image/ranks/7tcrest1.png" alt="rank icon image">&ensp;Brigadier General: 1,340,000<br>
                <img src="https://pawngame.com/static/website/landing/image/ranks/7tcrest2.png" alt="rank icon image">&ensp;Major General: 1,840,000<br>
                <img src="https://pawngame.com/static/website/landing/image/ranks/7tcrest3.png" alt="rank icon image">&ensp;Lieutenant General: 2,340,000<br>
                <img src="https://pawngame.com/static/website/landing/image/ranks/7tcrest4.png" alt="rank icon image">&ensp;General: 2,840,000<br>
                <img src="https://pawngame.com/static/website/landing/image/ranks/7tcrest5.png" alt="rank icon image">&ensp;General of the Army: 4,000,000<br>
                <img src="https://pawngame.com/static/website/landing/image/ranks/pawn.png" alt="rank icon image">&ensp;Member of the Pawn Elite: Top 10<br>

                <img src="https://pawngame.com/static/website/landing/image/ranks/admin.png" alt="rank icon image">&ensp;Administrator <br>
                <img src="https://pawngame.com/static/website/landing/image/ranks/mod.png" alt="rank icon image">&ensp;Moderator
            </p>
        </td>
    </tr>
    </tbody>
</table>

<?php require('src/elements/footer.php') ?>
</body>
</html>
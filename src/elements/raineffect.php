<?php

/*
 * Copyright © 2022  Alex
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

$rain_effect_image = "https://d1h3njoxaty54c.cloudfront.net/website/snow.png";
?>
<style>
    @-webkit-keyframes spin-360 {
        0% {
            -webkit-transform: rotate(0deg);
            transform: rotate(0deg);
        }
        100% {
            -webkit-transform: rotate(360deg);
            transform: rotate(360deg);
        }
    }
    @keyframes spin-360 {
        0% {
            -webkit-transform: rotate(0deg);
            transform: rotate(0deg);
        }
        100% {
            -webkit-transform: rotate(360deg);
            transform: rotate(360deg);
        }
    }
    @-webkit-keyframes cascade-down {
        0% {
            -webkit-transform: translateY(0, -8px);
            transform: translateY(0, -8px);
        }
        100% {
            -webkit-transform: translate(0, 103vh);
            transform: translate(0, 103vh);
        }
    }
    @keyframes cascade-down {
        0% {
            -webkit-transform: translateY(0, -8px);
            transform: translateY(0, -8px);
        }
        100% {
            -webkit-transform: translate(0, 103vh);
            transform: translate(0, 103vh);
        }
    }
    .Dashboard {
        display: flex;
        flex-direction: column;
        min-height: 0;
        pointer-events: none;
    }
    .Dashboard-members {
        position: absolute;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        -webkit-transform: translate3d(0, 0, 0);
        transform: translate3d(0, 0, 0);
        overflow: hidden;
    }
    .Dashboard-members:before {
        content: '';
        display: block;
        top: 0;
        left: 0;
        right: 0;
        height: 0;
        position: absolute;
        z-index: 990;
    }
    .Dashboard-members-dot {
        position: relative;
        -webkit-animation: 26s cascade-down linear;
        animation: 26s cascade-down linear;
        -webkit-animation-fill-mode: forwards;
        animation-fill-mode: forwards;
        -webkit-animation-iteration-count: 12;
        animation-iteration-count: 12;
        left: 5%;
    }
    .Dashboard-members-dot:nth-child(0) {
        left: 5%;
        top: 0;
        -webkit-animation-delay: 0ms;
        animation-delay: 0ms;
    }
    .Dashboard-members-dot:nth-child(0) .Member {
        -webkit-animation-delay: 0ms;
        animation-delay: 0ms;
    }
    .Dashboard-members-dot:nth-child(1) {
        left: 90%;
        top: 0;
        -webkit-animation-delay: 2500ms;
        animation-delay: 2500ms;
    }
    .Dashboard-members-dot:nth-child(1) .Member {
        -webkit-animation-delay: 2500ms;
        animation-delay: 2500ms;
    }
    .Dashboard-members-dot:nth-child(2) {
        left: 80%;
        top: 0;
        -webkit-animation-delay: 5000ms;
        animation-delay: 5000ms;
    }
    .Dashboard-members-dot:nth-child(2) .Member {
        -webkit-animation-delay: 5000ms;
        animation-delay: 5000ms;
    }
    .Dashboard-members-dot:nth-child(3) {
        left: 50%;
        top: 0;
        -webkit-animation-delay: 7500ms;
        animation-delay: 7500ms;
    }
    .Dashboard-members-dot:nth-child(3) .Member {
        -webkit-animation-delay: 7500ms;
        animation-delay: 7500ms;
    }
    .Dashboard-members-dot:nth-child(4) {
        left: 60%;
        top: 0;
        -webkit-animation-delay: 10000ms;
        animation-delay: 10000ms;
    }
    .Dashboard-members-dot:nth-child(4) .Member {
        -webkit-animation-delay: 10000ms;
        animation-delay: 10000ms;
    }
    .Dashboard-members-dot:nth-child(5) {
        left: 25%;
        top: 0;
        -webkit-animation-delay: 12500ms;
        animation-delay: 12500ms;
    }
    .Dashboard-members-dot:nth-child(5) .Member {
        -webkit-animation-delay: 12500ms;
        animation-delay: 12500ms;
    }
    .Dashboard-members-dot:nth-child(6) {
        left: 18%;
        top: 0;
        -webkit-animation-delay: 15000ms;
        animation-delay: 15000ms;
    }
    .Dashboard-members-dot:nth-child(6) .Member {
        -webkit-animation-delay: 15000ms;
        animation-delay: 15000ms;
    }
    .Dashboard-members-dot:nth-child(7) {
        left: 85%;
        top: 0;
        -webkit-animation-delay: 17500ms;
        animation-delay: 17500ms;
    }
    .Dashboard-members-dot:nth-child(7) .Member {
        -webkit-animation-delay: 17500ms;
        animation-delay: 17500ms;
    }
    .Dashboard-members-dot:nth-child(8) {
        left: 75%;
        top: 0;
        -webkit-animation-delay: 20000ms;
        animation-delay: 20000ms;
    }
    .Dashboard-members-dot:nth-child(8) .Member {
        -webkit-animation-delay: 20000ms;
        animation-delay: 20000ms;
    }
    .Dashboard-members-dot:nth-child(9) {
        left: 30%;
        top: 0;
        -webkit-animation-delay: 22500ms;
        animation-delay: 22500ms;
    }
    .Dashboard-members-dot:nth-child(9) .Member {
        -webkit-animation-delay: 22500ms;
        animation-delay: 22500ms;
    }

    .Member {
        position: absolute;
        display: block;
        width: 20px;
        height: 20px;
        margin-top: -20px;
        border-radius: 100%;
        background-image: url("<?php echo $rain_effect_image ?>");
        background-size: 100%;
        -webkit-animation: spin-360 2000ms infinite linear;
        animation: spin-360 2000ms infinite linear;
    }
</style>

<div class="Dashboard">

    <div class="Dashboard-members">
        <div class="Dashboard-members-dot">
            <div class="Member"></div>
        </div>
        <div class="Dashboard-members-dot">
            <div class="Member"></div>
        </div>
        <div class="Dashboard-members-dot">
            <div class="Member"></div>
        </div>
        <div class="Dashboard-members-dot">
            <div class="Member"></div>
        </div>
        <div class="Dashboard-members-dot">
            <div class="Member"></div>
        </div>
        <div class="Dashboard-members-dot">
            <div class="Member"></div>
        </div>
        <div class="Dashboard-members-dot">
            <div class="Member"></div>
        </div>
        <div class="Dashboard-members-dot">
            <div class="Member"></div>
        </div>
        <div class="Dashboard-members-dot">
            <div class="Member"></div>
        </div>
        <div class="Dashboard-members-dot">
            <div class="Member"></div>
        </div>
        <div class="Dashboard-members-dot">
            <div class="Member"></div>
        </div>
        <div class="Dashboard-members-dot">
            <div class="Member"></div>
        </div>
        <div class="Dashboard-members-dot">
            <div class="Member"></div>
        </div>
        <div class="Dashboard-members-dot">
            <div class="Member"></div>
        </div>
        <div class="Dashboard-members-dot">
            <div class="Member"></div>
        </div>
        <div class="Dashboard-members-dot">
            <div class="Member"></div>
        </div>
        <div class="Dashboard-members-dot">
            <div class="Member"></div>
        </div>
        <div class="Dashboard-members-dot">
            <div class="Member"></div>
        </div>
    </div>
</div>
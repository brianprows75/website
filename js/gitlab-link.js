/*
 * Copyright (c) 2022  Alex
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
{
    let link_element = document.getElementById("gitlab-link");
    let repo_url = "https://gitlab.com/pawnstudios/web/website/-/tree/master";
    let document_url = document.URL;
    let domain = document.domain;
    let url_parts = document_url.split(domain);

    if (url_parts.length < 2 || url_parts[1] === "/") {
        link_element.href = repo_url + "/home.php"
    } else {
        link_element.href = repo_url + url_parts[1];
    }
}